package model

class Shoe(var sku: Int, var mark: String) {

    init {
        println("SKU ID: $sku")
    }

    var size: Int = 34
    set(value) {
        if (value>=34)
            field = value
        else
            field = 34
    }
    get() = field
    var color: String = "White"
    var model: String = "Boots"
    set(value) {
        if (value.equals("Tenis"))
            field = "Boots"
        else
            field = value
    }
        get() = field
}