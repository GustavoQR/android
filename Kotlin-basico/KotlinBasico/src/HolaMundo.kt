import model.Camera
import model.Movie
import model.Shoe
import java.lang.NullPointerException

//Tenemos primitivos, objetos (variables)
//Todas las variables son objetos (hay que tratar que sea asi)

var n = 3
const val N = "NAME"  //mala practica
//Funciones puras
fun main(args: Array<String>) {
    println("Hola Mundo")
    println(1+1)
    println(3-1)
    println(4*4)
    println(10/2)
    println(7%2)

    //Tratamos las variables como objetos
    val a = 4
    val b = 2
    println(a.plus(b))
    println(a.minus(b))
    println(a.times(b))
    println(a.div(b))

    //Tipos de variables
    //Kotlin obedece a la programacion funcional (Inmutabilidad -> Que los valores no cambien)
    // var , val , const
    // Tipos de variables, changeables y unchangeables
    //var (changeables) val (unchangeables)

    //unchangeable
    //const El valor se determina en tiempo de compilacion
    //val El valor se puede determinar en tiempo de ejecucion

    val name = args[0]  //Tiempo de ejecucion
    //const tiempo de compilacion
    println(name)
    println(N)

    //Cadenas

    val nombre = "Gustavo"
    val apellido: String = "Quispe"
    //Salto de linea
    val nombreApellido = "Gustavo\nQuispe"
    println("Tu nombre es: $nombre $apellido")
    println("Tu nombre es: $nombreApellido")
    //Raw String
    val parrafo = """
            ** sdasdasdkjasndkjhdkljadahdkljadkjhasdhasdhasdk
           ** dsadlnasdnkjasdnkljnasdkljbasdkjbasdhbasdhjasbdhj
            ** dlsandkjansdkjad dnasdkjnasdkj adnkjasdnkjasdndas
            ** dsdnkjasbd dasndsadn asdasdas adasdasd dasdasdas
            ** dasdasd  dasdasdasd  asdasd.
        """.trimIndent()
    println(parrafo.trimMargin("** "))

    //Rango
    val oneToHundred = 1..10
    for (i in oneToHundred){
        println(i)
    }

    for(letra in 'A'..'C'){
        println(letra)
    }

    //operadores logicos
    val numero = 2
    if (numero.equals(2)){
        println("Si son iguales")
    }else{
        println("No, son iguales")
    }

    //When switch
    when(numero){
        in 1..5 -> println("Si esta entre 1 y 5")
        in 1..3 -> println("Si esta entre 1 y 3")
        !in 5..10 -> println("No, no esta entre el 5 y el 10")
        else -> println("No esta en alguno de los anteriores")
    }

    var i = 1
    //while contadores
    while (i <= 10){
        println("mensaje $i")
        i++
    }
    i = 1
    do{
        println("mensaje: $i")
        i++
    }while (i<=10)

    //Kotlin es Null Safety

    /*  try{
          var compute: String?
          compute = null
          var longitud:Int = compute!!.length
      }catch (e: NullPointerException){
          println("Ingresa un valor, no aceptamos nulos")
      }*/

    //Llamada Segura
    var compute: String? = null
    var longitud: Int? = compute?.length
    println("Longutud: $longitud")

    //Operador Elvis
    var teclado: String? = null
    val longitudTeclado: Int = teclado?.length ?: 0
    println("Longutud del teclado: $longitudTeclado")

    val listWithNulls: List<Int?> = listOf<Int?>(7,null, null,4)
    println("Lista con Null: ${listWithNulls}")

    val listWithOutNulls: List<Int?> = listWithNulls.filterNotNull()
    println(listWithOutNulls)

    //Arreglos Array  / ArrayOf cualquier tipo de dato

    /*
    arrayOf Almacena objetos y intArray Datos tipo primitivo
     */
    val countries = arrayOf("India", "Mexico","Colombia","Argentina","Peru")
    val days = arrayOf<String>("Lunes","Martes","Miercoles")



    var arrayObject = arrayOf(1,2,3)
    var intPrimitive: IntArray = arrayObject.toIntArray()

    val suma = arrayObject.sum()
    println("La suma del array es $suma")
    arrayObject = arrayObject.plus(4)
    for (a in arrayObject){
        println("Array: $a")
    }

    arrayObject = arrayObject.reversedArray()
    for (a in arrayObject){
        println("Array reversa: $a")
    }

    arrayObject.reverse()

    //Expreciones
    var x = 5
    println("X es igual a 5? ${x==5}")//Valor booleano

    var mensaje = "El valor x es $x"
    x++
    println("${mensaje.replace("es","fue")}, x es igual a: $x")

    //en kotlin siempre se devulve un valor

    println("Raiz cuadrada de: ${Math.sqrt(4.0)}")

    val numbers = intArrayOf(6,6,23,9,2,3,2)
    println("El promedio de los numeros es: ${averageNumbers(numbers,2)}")
    println("${evaluate('+')}")

    //Lambdas
    val saludo = { println("Hola Mundo") }

    saludo()
    //{instrucciones -> sentencias}
    var plus = { a:Int, b:Int, c: Int -> a+b+c }
    val result = plus(3,4,5)
    println(result)
    println(plus(1,2,3))
    //Lo mismo de otra forma
    println({a: Int, b: Int, c: Int -> a+b+c}(7,8,9))

    val calculateNumber = { n:Int ->
        when (n){
            in 1..3 -> println("tu numero esta entre 1 y 3")
            in 4..7 -> println("tu numero esta entre 4 y 7")
            in 8..10 -> println("tu numero esta entre 8 y 10")
        }
    }
    println(calculateNumber(6))
//En kotlin todos los valores son publicos por defecto

    /*
   public todo acceso
   private acceso solo dentro de la clase
   protected acceso solo dentro de la clase y las clases que hereden
   internal acceso entre modulos
    */
    val camera = Camera()
    camera.turnOn()
    println(camera.getCameraStatus())

    camera.setResolution(1080)
    println("Resolution: ${camera.getResolution()}")

    var shoe = Shoe(123456,"Praga")
    shoe.size = 37
    println(shoe.size)

    shoe.model = "Zapatos"
    println(shoe.model)

    println(shoe.mark)

    val movie = Movie("Coco", "Pixar", 120)
    println("MOVIE")
    println(movie.title)
    println(movie.creator)
    println("${movie.duration} min")





}

fun evaluate(character: Char = '=', number: Int = 2): String{
    return "${number} es ${character}"
}

fun averageNumbers(numbers: IntArray, n: Int): Int {
    //Promedio de los numeros
    var sum = 0
    for (num in numbers){
        sum += num
    }

    return (sum / numbers.size)+n
}